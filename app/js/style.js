$(document).ready(function() {
  /*меню переходы*/

  $('header .menu li a').on('click', function(event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id = $(this).attr('href'),
      //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;

    //анимируем переход на расстояние - top за 1000 мс
    $('html, body')
      .stop()
      .animate(
        {
          scrollTop: top - 100,
          easing: 'easein',
        },
        1000
      );
  });
  $('.logo').on('click', function(event) {
    $('html, body')
      .stop()
      .animate(
        {
          scrollTop: 0,
          easing: 'easein',
        },
        1000
      );
  });

  $('.btn-menu-mob').on('click', function() {
    var el = $(this);
    el.toggleClass('active')
      .parent()
      .find('nav')
      .toggleClass('active')
      .slideToggle();
    $(body).toggleClass('hidden');
  });

  /*анимация services */
  var offset;
  if ($(window).width() > 767) {
    offset = '60%';
  } else {
    offset = '10%';
  }
  $('#our-services').viewportChecker({
    offset: offset,
    callbackFunction: function() {
      $('.svg-circle').each(function() {
        var thisCircle = $(this).find('circle');
        var svgCircleTl = new TimelineMax();

        var thisPercent = $(this).attr('data-percent');
        console.log(thisPercent);

        svgCircleTl
          .set(thisCircle, { visibility: 'visible', drawSVG: '0%' })
          .fromTo(
            thisCircle,
            3,
            { drawSVG: '0%' },
            { drawSVG: thisPercent + '%', ease: Power4.easeOut }
          );
      });
    },
  });

  /*feedback message */
  $('.fixed-feddback .message-btn').on('click', function() {
    $(this).toggleClass('active');
    $(this)
      .parent('.message')
      .find('.fixed-feedback-form')
      .slideToggle(300);
  });

  /*слайдер главный по работам */
  var swiper = new Swiper('.works-swiper-container', {
    simulateTouch: false,
    speed: 1000,
    pagination: {
      el: '.swiper-pagination',
      type: 'fraction',
    },
    navigation: {
      nextEl: '.works-swiper-button-next',
      prevEl: '.works-swiper-button-prev',
    },
  });
  /* слайдеры в слайдере */
  $('.our-works-slide').each(function(index, elem) {
    var $this = $(this);
    $this
      .find('.picture-gallery-thumbs')
      .addClass('picture-gallery-thumbs-' + index);
    $this.find('.picture-gallery-top').addClass('picture-gallery-top-' + index);

    $this.find('.swiper-button-prev').addClass('btn-prev-' + index);
    $this.find('.swiper-button-next').addClass('btn-next-' + index);

    var galleryThumbs = new Swiper('.picture-gallery-thumbs-' + index, {
      spaceBetween: 0,
      slidesPerView: 6,
      freeMode: true,
      watchSlidesVisibility: true,
      watchSlidesProgress: true,
      breakpoints: {
        768: {
          slidesPerView: 4,
        },
        580: {
          slidesPerView: 3,
        },
      },
    });
    var galleryTop = new Swiper('.picture-gallery-top-' + index, {
      spaceBetween: 10,
      //simulateTouch: false,
      navigation: {
        nextEl: '.btn-next-' + index,
        prevEl: '.btn-prev-' + index,
      },
      thumbs: {
        swiper: galleryThumbs,
      },
    });
  });

  /* слайдер feedback */
  var swiper = new Swiper('.feedback-slider', {
    navigation: {
      nextEl: '.feedback-swiper-button-next',
      prevEl: '.feedback-swiper-button-prev',
    },
  });
});
